#!/bin/bash
time_of_day(){ 
a="Доброе утро" 
b="Добрый день" 
c="Добрый вечер"
d="Доброй ночи"

HT="$1"
  if [ -z  "$HT" ]; then
     echo "Ошибка "   >&2
         return 1
   fi
   if  [ 0 -gt "$HT" ] || [ "$HT" -gt 23 ]; then
      echo "Error! arg not in [0, 23] "  >&2
      return 2
   fi
   if [ "$HT" -le 5 -o "$HT" -ge 23 ]; then 
      echo  "$d" 
   elif [ "$HT" -le 12 ]; then 
      echo "$a" 
   elif [ "$HT" -le 17 ]; then 
     echo "$b"
   elif [ "$HT" -le 22 ]; then 
     echo "$c"   
   else  
        return 1  
   fi
}
testing(){ 
        set -x   
        [ "$(time_of_day 10)" = "Доброе утро" ]
        [ "$(time_of_day 0)" = "Доброй ночи" ]
        [ "$(time_of_day 22)" = "Добрый вечер" ]
        ! time_of_day -
        ! time_of_day 25
        ! time_of_day -1
 } 
main() {
    HT=$(date +'%H')
#    M=$(date +'%M')
#     if [ '$M' != 00 ]; then
#          HT=$(( $H+1 )) #если минут не 00, то прибавляет 1 час
#     else
#          HT='$H'
#     fi
 time_of_day $HT
}

if [ "$1" = "--test" ]; then
    testing
else
main 
fi 

