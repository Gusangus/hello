#!/bin/bash 
set -x -e

testing () {
        datetime=$1
        expect="$2"
     
        date --set="$datetime"
        [ "$(./hello.sh)" = "$expect" ]
}
testing 5:00 "Доброй ночи"
testing 00:00 "Доброй ночи" 
testing 22:00  "Добрый вечер" 
